FROM node:16-alpine

# Cài đặt dockerize để sử dụng biến môi trường từ file .env
RUN apk add --no-cache --virtual .build-deps \
    curl \
    && curl -sSL https://github.com/jwilder/dockerize/releases/download/v0.6.1/dockerize-alpine-linux-amd64-v0.6.1.tar.gz | tar -xz -C /usr/local/bin \
    && apk del .build-deps

WORKDIR /fe

COPY package.json package-lock.json ./

RUN npm install

COPY . .

RUN npm run build

# Cài đặt serve để phục vụ ứng dụng build
RUN npm install -g serve

# Copy file .env
COPY .env .env

# Sử dụng dockerize để chạy serve với port từ biến môi trường
CMD ["sh", "-c", "serve -s build -l ${REACT_LOCAL_PORT:-3001}"]

EXPOSE ${REACT_LOCAL_PORT:-3001}
