import { useState } from "react";
import "./App.css";
import Login from "./Login";
import Register from "./Register";

function App() {
  const [isLogin, setIsLogin] = useState(false)
  const userInfo = localStorage.getItem("userInfoTest")
  const [isSucess, setIsSuccess] = useState(!!userInfo)
  if (isSucess || userInfo) {
    return <div style={{ color: 'white', margin: 30 }}>
      <h1>Login Success</h1>
      <h1>username: {JSON.parse(userInfo)?.username}</h1>
      <button style={{ width: 80 }} onClick={() => {
        localStorage.removeItem("userInfoTest")
        localStorage.removeItem("accessToken")

        setIsSuccess(false)
      }}>Logout</button>
    </div>
  }
  return (
    <>
      {isLogin ? <Login handleChangePage={() => setIsLogin(false)} onSuccess={() => setIsSuccess(true)} /> : <Register handleChangePage={() => setIsLogin(true)} />}
    </>
  );
}

export default App;
