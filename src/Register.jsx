import { useState } from "react";
import axios from "axios"

function Register({ 
    handleChangePage
}) {
    const initialValues = {
        username: "",
        fullname: "",
        password: "",
        confirmPassword: "",
    };
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [msg, setMsg] = useState('')
    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormValues({ ...formValues, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const err = validate(formValues)
        setFormErrors(err)
        if (Object.keys(err).length) return
        try {
           await axios.post('http://localhost:5000/account', formValues)
           setMsg('success') 
           handleChangePage()
        } catch (error) {
           setMsg(error?.response?.data?.error || "error") 
        }
    };

    const validate = (values) => {
        const errors = {};
        if (!values.username) {
            errors.username = "Username is required!";
        }
        if (!values.fullname) {
            errors.email = "Fullname is required!";
        }
        if (!values.password) {
            errors.password = "Password is required";
        } else if (values.password.length < 4) {
            errors.password = "Password must be more than 4 characters";
        } else if (values.password.length > 10) {
            errors.password = "Password cannot exceed more than 10 characters";
        }
        if (values.password !== values.confirmPassword) {
            errors.confirmPassword = "Those passwords didn’t match. Try again.";
        }
        return errors;
    };

    return (
        <>
            <div className="bgImg"></div>
            <div className="container">
                {msg && <div className={`ui message error`}>{msg}</div>}
                <form onSubmit={handleSubmit}>
                    <h1>Register</h1>
                    <div className="ui divider"></div>
                    <div className="ui form">
                        <div className="field">
                            <label>Username</label>
                            <input
                                type="text"
                                name="username"
                                placeholder="Choose a username"
                                value={formValues.username}
                                onChange={handleChange}
                            />
                        </div>
                        <p>{formErrors.username}</p>
                        <div className="field">
                            <label>Fullname</label>
                            <input
                                type="text"
                                name="fullname"
                                placeholder="fullname"
                                value={formValues.fullname}
                                onChange={handleChange}
                            />
                        </div>
                        <p>{formErrors.fullname}</p>
                        <div className="field">
                            <label>Password</label>
                            <input
                                type="password"
                                name="password"
                                placeholder="Password"
                                value={formValues.password}
                                onChange={handleChange}
                            />
                        </div>
                        <p>{formErrors.password}</p>
                        <div className="field">
                            <label>Confirm Password</label>
                            <input
                                type="password"
                                name="confirmPassword"
                                placeholder="Confirm password"
                                value={formValues.confirmPassword}
                                onChange={handleChange}
                            />
                        </div>
                        <p>{formErrors.confirmPassword}</p>
                        <button className="fluid ui button blue">Submit</button>
                    </div>
                </form>
                <div className="text" onClick={handleChangePage}>
                    Already have an account? <span>Login</span>
                </div>
            </div>{" "}
        </>
    );
}

export default Register;
